"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _ambtecLogger = _interopRequireDefault(require("ambtec-logger"));

// @ts-ignore
class SocketIo {
  constructor(RED, callbacks) {
    this.prod = void 0;
    this.socketIo = void 0;
    this.callbacks = void 0;

    const {
      Server
    } = require("socket.io");

    const corsOrigin = process.env.SOCKETIO_CORS_ORIGINS ? process.env.SOCKETIO_CORS_ORIGINS.split(",") : [];
    const socketEndpoint = process.env.SOCKETIO_ENDPOINT ? process.env.SOCKETIO_ENDPOINT : "/socket.io";
    const corsHeaders = process.env.SOCKETIO_CORS_HEADERS ? process.env.SOCKETIO_CORS_HEADERS.split(",") : [];
    const corsCredentials = "true" === process.env.SOCKETIO_CORS_CREDENTIALS;
    this.prod = "true" === process.env.PRODUCTION;
    this.callbacks = callbacks;
    let ioOptions = {};

    if (corsOrigin.length) {
      ioOptions = {
        cors: {
          origin: corsOrigin,
          methods: ["GET", "POST"],
          allowedHeaders: corsHeaders,
          credentials: corsCredentials
        }
      };
    }

    this.socketIo = new Server(RED.server, ioOptions);
    this.socketIo.serveClient(true);
    this.socketIo.path(socketEndpoint);
    this.socketIo.on("connection", this.handleConnect.bind(this));

    _ambtecLogger.default.debug({
      breadcrumbs: ["socket.io", "lib", "SocketIo"],
      message: "Init"
    });
  }

  handleConnect(socket) {
    const loginTimeout = process.env.SOCKETIO_LOGIN_TIMEOUT ? process.env.SOCKETIO_LOGIN_TIMEOUT : 2000;

    _ambtecLogger.default.debug({
      breadcrumbs: ["socket.io", socket.id, "connection"],
      message: "Socket connected"
    });

    if (this.callbacks.connect) this.callbacks.connect(socket); // Set rules

    for (const ioEvent of this.getEvents()) {
      this.addEventListener(socket, ioEvent);
    }

    socket.on("disconnect", this.handleDisconnect.bind(this, socket));
    socket.joinedRooms = [];
    socket.connectionTimeout = setTimeout(this.handleConnectTimeout.bind(this, socket), loginTimeout); // Request login code by client

    this.socketIo.sockets.sockets.get(socket.id).emit("authenticate", "iam authentication is required");
  }

  handleConnectTimeout(socket) {
    if (this.callbacks.timeout) this.callbacks.timeout(socket);
  }

  handleDisconnect(socket) {
    if (this.callbacks.disconnect) this.callbacks.disconnect(socket);
    const localSocket = "string" === typeof socket ? this.getSocketById(socket) : socket;
    clearTimeout(localSocket.connectionTimeout);
    localSocket.disconnect();

    _ambtecLogger.default.debug({
      breadcrumbs: ["socket.io", socket.id, "handleDisconnect"],
      message: "disconnected"
    });
  }

  handleListenerEvent(socket, eventId, data) {
    if (this.callbacks.eventListener) this.callbacks.eventListener(socket, eventId, data);
  }

  addEventListener(socket, eventId) {
    if (!eventId.length) return;

    _ambtecLogger.default.debug({
      breadcrumbs: ["socket.io", socket.id, eventId],
      message: "Bind event listener"
    });

    socket.off(eventId, this.handleListenerEvent.bind(this, socket, eventId));
    socket.on(eventId, this.handleListenerEvent.bind(this, socket, eventId));
  }

  getEvents() {
    return process.env.SOCKETIO_EVENTS ? process.env.SOCKETIO_EVENTS.split(",") : [];
  }

  getSocketById(socketId) {
    return this.socketIo.sockets.sockets.get(socketId);
  }

  closeSocket(socket) {
    const localSocket = "string" === typeof socket ? this.getSocketById(socket) : socket;

    _ambtecLogger.default.debug({
      breadcrumbs: ["socket.io", localSocket.id, "closeSocket"]
    });

    this.handleDisconnect(localSocket);
  }

  emitToAudience(event, payload) {
    this.socketIo.emit(event, payload);
  }

  emitToRoom(room, event, payload) {
    this.socketIo.to(room).emit(event, payload);
  }

  joinRoom(socket, room) {
    const localSocket = "string" === typeof socket ? this.getSocketById(socket) : socket;
    localSocket.joinedRooms.push(room);
    localSocket.join(room);
  }

  hasRoom(socket, room) {
    const localSocket = "string" === typeof socket ? this.getSocketById(socket) : socket;
    return localSocket.joinedRooms.indexOf(room) > -1;
  }

  leaveRoom(socket, room) {
    const localSocket = "string" === typeof socket ? this.getSocketById(socket) : socket;
    localSocket.joinedRooms = localSocket.joinedRooms.splice(localSocket.joinedRooms.indexOf(room), 1);
    localSocket.leave(room);
  }

}

var _default = SocketIo;
exports.default = _default;