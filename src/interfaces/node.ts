declare var process: {
  env: {
    NODE_ENV: string;
    PRODUCTION: string;
    SOCKETIO_CORS_ORIGINS: string;
    SOCKETIO_CORS_HEADERS: string;
    SOCKETIO_CORS_CREDENTIALS: string;
    SOCKETIO_LOGIN_TIMEOUT: number;
    SOCKETIO_ENDPOINT: string;
    SOCKETIO_EVENTS: string;
    IAM_SERVICE_HOST: string;
    IAM_ADDRESS_LOGOUT: string;
    IAM_ADDRESS_TOKEN: string;
  };
  log: Log;
  socketIo: SocketIo;
  iam: Iam;
  iamSocketMap: SocketLocals;
  socketLocals: IamSocketMap;
  on: Function;
  emit: Function;
};

interface Log {
  trace: Function;
  debug: Function;
  info: Function;
  success: Function;
  notice: Function;
  warning: Function;
  error: Function;
  crit: Function;
  alert: Function;
  emerg: Function;
}

interface IamSocketMap {
  set: Function;
  get: Function;
  has: Function;
  delete: Function;
}

interface SocketLocals {
  set: Function;
  get: Function;
  has: Function;
  delete: Function;
}

interface Iam {
  send: Function;
  logout: Function;
  getIamConfiguration: Function;
  keepAlive: Function;
}

interface SocketIo {
  getSocketById: Function;
  closeSocketById: Function;
  emitToRoom: Function;
  emit: Function;
}
