import {
  NodeRedApp,
  EditorNodeProperties,
  EditorRED,
  NodeMessageInFlow,
} from "node-red";

import {
  TcpSocketConnectOpts,
  Socket,
  Server as NetServer,
  LookupFunction,
} from "node:net";

export interface CustomSocket extends Socket {
  id: string;
  join: Function;
  leave: Function;
  disconnect: Function;
  connectionTimeout: NodeJS.Timeout;
  joinedRooms: string[];
  iam: ResponseIamConfiguration;
}

export interface CustomSocketData {
  id: string;
  connectionTimeout: NodeJS.Timeout;
}

export interface CustomEditorNodeProperties extends EditorNodeProperties {
  path: string;
  rules: string[];
  overwriteroom: boolean;
  room: string;
  socketid: string;
}
export interface CustomPayload {
  [key: string]: any;
}

export interface CustomSocketMsg {
  [key: string]: any;
  payload: CustomPayload;
  socket: CustomSocketMsgSocket;
  iam: CustomSocketMsgIam;
}

export interface CustomSocketMsgSocket {
  emit: string;
  event: string;
  id: string;
}

export interface CustomSocketMsgIam {
  userId: string;
  token: string;
  client_id: string;
  tokenExpiresIn: string;
  session: string;
  resource_access: iamAccount;
  groups: string[];
  username: string;
}

export interface iamAccount {
  account: iamAccountRoles;
}

export interface iamAccountRoles {
  roles: string[];
}

export interface NodeJS {
  Process: CustomProcess;
}

export interface NodeJS {
  Process: CustomProcess;
}

export interface CustomProcess extends NodeJS.Process {
  io: any;
  iam: any;
  iamSocketMap: any;
  socketLocals: any;
}

export interface ResponseIamSocket {
  event: string;
  id: string;
}

export interface SocketPayload {
  payload: any;
  socket: ResponseIamSocket;
  iam: ResponseIamConfiguration;
}

export interface ResponseIamConfiguration {
  username: string;
  resource_access: string;
  groups: string[];
}
